#include <iostream>
#include <map>
#include <string>
#include "Money.h"

class CreditCard
{
	std::string ownerName, cardNumber;
	std::map<std::string, Money> transactions;

public:
	CreditCard(std::string owner = std::string(), std::string card = std::string());
	~CreditCard();

	void addTransactions(std::string, int, int);
	void addTransactions(std::string, const Money&);
	void printTransactions()const;

	std::string getOwnerName()const;
	std::string getCardNumber()const;
	std::map<std::string, Money> getTransactions()const;
	void setOwnerName(std::string);
	void setCardNumber(std::string);
	void setTransactions(std::map<std::string, Money>);

	friend std::istream& operator >>(std::istream&, CreditCard &);
	friend std::ostream& operator <<(std::ostream&, const CreditCard&);
};

