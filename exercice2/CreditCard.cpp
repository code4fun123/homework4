#include "CreditCard.h"

CreditCard::CreditCard(std::string owner, std::string card)
{
	this->cardNumber = card;
	this->ownerName = owner;
}

CreditCard::~CreditCard()
{
	this->cardNumber.clear();
	this->ownerName.clear();
	this->transactions.clear();
}

void CreditCard::addTransactions(std::string name, int  euros, int cents)
{
	std::pair<std::string, Money> pair(name, Money(euros, cents));
	this->transactions.insert(pair);
}

void CreditCard::addTransactions(std::string name, const Money& cost)
{
	std::pair<std::string, Money> pair(name, cost);
	this->transactions.insert(pair);
}

void CreditCard::printTransactions()const
{
	if (this->transactions.size() == 0)
		std::cout << " No transactions!\n";
	else
	{
		std::cout << std::endl;
		for (auto iterator : this->transactions)
			std::cout << "(" << iterator.first << ",  euros : " << iterator.second << ")\n";
	}
}


std::string CreditCard::getOwnerName()const
{
	return this->ownerName;
}

std::string CreditCard::getCardNumber()const
{
	return this->cardNumber;
}

std::map<std::string, Money> CreditCard::getTransactions() const
{
	return this->transactions;
}

void CreditCard::setOwnerName(std::string name)
{
	this->ownerName = name;
}

void CreditCard::setCardNumber(std::string card)
{
	this->cardNumber = card;
}

void CreditCard::setTransactions(std::map<std::string, Money> transactions)
{
	this->transactions = transactions;
}

std::istream& operator >> (std::istream& stream, CreditCard& card)
{
	std::cout << "Owner name: "; getline(stream, card.ownerName);
	std::cout << "Card number: "; getline(stream, card.cardNumber);
	return stream;

}
std::ostream& operator << (std::ostream& stream, const CreditCard& card)
{
	stream << "Owner: " << card.ownerName << ", card number: " << card.cardNumber << ", transactions:";
	card.printTransactions();
	return stream;
}