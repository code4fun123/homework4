#pragma once
#include"DiscountRate.h"
#include"Visit.h"
#include<map>
#include<utility>
#include<fstream>
#include<iomanip>

class BeautySaloon
{
	std::map<std::string,double> products;
	std::map<std::string,double> services;
	DiscountRate discount;

public:
	BeautySaloon();
	~BeautySaloon();

	void welcome()const;

	void visit(const Customer&);

	void checkout(Visit, std::map<std::string, double>);
};

