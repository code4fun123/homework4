#include"Visit.h"
#include"DiscountRate.h"
#include"BeautySaloon.h"
#include<iostream>

int main() {

	BeautySaloon newSaloon;

	Customer customer;
	std::cout << "\nHello! To continue, please enter the following data:\n";
	std::cin >> customer;

	newSaloon.visit(customer);

	return 0;
}