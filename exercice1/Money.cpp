#include "Money.h"


Money::Money(int euros, int centimes)
{
	this->euros = euros;
	this->centimes = centimes;
	Money::convertCentimesToEuros(*this);
}

Money::~Money()
{

}

unsigned int Money::getEuros()const
{
	return this->euros;
}

unsigned int Money::getCentimes()const
{
	return this->centimes;
}

void Money::setEuros(unsigned int euros) {
	this->euros = euros;
}

void Money::setCentimes(unsigned int centimes) {
	this->centimes = centimes;
}

bool Money::operator < (const Money money)const
{	
		if (this->euros == money.euros)
		{
			return this->centimes < money.centimes;
		}
		else
		{
			return this->euros < money.euros;
		}
}

bool Money::operator>(const Money money)const
{
	return !(*this<money);
}

bool Money::operator != (const Money money)const
{
	return !(*this==money);
}

bool Money::operator == (Money m)const
{
	if (euros == m.euros && centimes == m.centimes)
		return 1;
	return 0;
}

Money Money::operator =(const Money money)
{
	euros = money.euros;
	centimes = money.centimes;
	return *this;
}

std::ostream &operator <<(std::ostream &stream, const Money& money)
{
	stream << "("<<money.euros << ',' << money.centimes<<") Euros";
	return stream;
}

std::istream &operator >>(std::istream &stream, Money &money)
{

	std::cout << " euros: ";
	stream >> money.euros;
	std::cout << " centimes: ";
	stream>> money.centimes;
	Money::convertCentimesToEuros(money);
	return stream;
}

Money operator + (const Money money1, const Money money2)
{
	return Money(money1.euros + money2.euros, money1.centimes + money2.centimes);
}


Money operator - (const Money money1, const Money money2)
{
	if (money1.euros<money2.euros)
		throw new std::exception("Invalid operation!");

	return Money(money1.euros - money2.euros, money1.centimes - money2.centimes);
}

Money operator * (const Money money1, const Money money2)
{
	return Money(money1.euros * money2.euros, money1.centimes * money2.centimes);
}

Money operator / (const Money money1, const Money money2)
{
	if (money2.euros == 0 && money2.centimes == 0)
		throw new std::exception("Invalid operation");
	unsigned int nr_centimes1 = Money::convertMoneyToCentimes(money1);
	std::cout << nr_centimes1 << " \n";
	unsigned int nr_centimes2 = Money::convertMoneyToCentimes(money2);
	std::cout << nr_centimes2 << " \n";
	return Money(0, ((double)nr_centimes1 / (double)nr_centimes2) * 100);

}

int Money::convertMoneyToCentimes(const Money & money)
{
	return money.centimes + money.euros * 100;
}

void Money::convertCentimesToEuros(Money &money)
{
	while (money.centimes >= 100)
	{
		money.euros++;
		money.centimes = money.centimes - 100;
	}

}



