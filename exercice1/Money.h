#pragma once
#include <iostream>
#include<exception>

class Money
{
private:
	unsigned int centimes, euros;
	static int convertMoneyToCentimes(const Money&);
	static void convertCentimesToEuros(Money&);

public:
	Money(int euros = 0, int centimes = 0);
	~Money();

	unsigned int getEuros()const;
	unsigned int getCentimes()const;
	void setEuros(unsigned int);
	void setCentimes(unsigned int);

	bool operator < (const Money)const;
	bool operator > (const Money)const;
	bool operator != (const Money)const;
	bool operator ==(const Money)const;
	Money operator=(const Money);

	friend std::ostream & operator << (std::ostream &flux,const Money&);
	friend std::istream & operator >> (std::istream &flux,Money&);
	friend Money operator +(const Money, const Money);
	friend Money operator -(const Money, const Money);
	friend Money operator /(const Money, const Money);
	friend Money operator *(const Money, const Money);
};
